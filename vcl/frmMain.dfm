object Form1: TForm1
  Left = 0
  Top = 0
  Caption = 'Form1'
  ClientHeight = 435
  ClientWidth = 589
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object btnSend: TSpeedButton
    Left = 479
    Top = 60
    Width = 74
    Height = 25
    Caption = 'Send'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
    OnClick = btnSendClick
  end
  object rdoMethod: TRadioGroup
    Left = 24
    Top = 21
    Width = 121
    Height = 33
    Columns = 2
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ItemIndex = 1
    Items.Strings = (
      'GET'
      'POST')
    ParentFont = False
    TabOrder = 0
  end
  object edUrl: TEdit
    Left = 24
    Top = 60
    Width = 449
    Height = 27
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 1
    Text = 'http://localhost:8080/etax/postSignedTaxInvoice'
  end
  object Memo1: TMemo
    Left = 0
    Top = 112
    Width = 589
    Height = 323
    Align = alBottom
    Color = clBlack
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clLime
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 2
    StyleElements = []
  end
  object XMLDocument1: TXMLDocument
    Active = True
    Left = 312
    Top = 288
  end
end
