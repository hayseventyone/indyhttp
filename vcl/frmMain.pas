unit frmMain;

interface

uses
  Controller.Request,
  //
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.Buttons, Vcl.StdCtrls, Vcl.ExtCtrls,
  Xml.xmldom, Xml.XMLIntf, Xml.XMLDoc;

type
  TForm1 = class(TForm)
    rdoMethod: TRadioGroup;
    edUrl: TEdit;
    btnSend: TSpeedButton;
    XMLDocument1: TXMLDocument;
    Memo1: TMemo;
    procedure btnSendClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

procedure TForm1.btnSendClick(Sender: TObject);
var
  strPath: String;
  Stream: TStream;
begin
  try
    Stream := TStringStream.Create;

    try
      if PromptForFileName(strPath, '', '', '', '', false) then begin
        Memo1.Lines.Add(rdoMethod.ToString + ':' +edUrl.Text)   ;
        SendPostData(edUrl.Text, strPath, Stream);
        XMLDocument1.LoadFromStream(Stream, xetUTF_8);
        XMLDocument1.SaveToFile('./signed.xml');
        Memo1.Lines.Add('Complete : /signed.xml')
      end;
    Except
      on E: Exception do begin
        Memo1.Font.Color := clRed;
        Memo1.Lines.Add(E.Message)
      end;

    end;

  finally
    Stream.Free;
  end;

end;

end.
