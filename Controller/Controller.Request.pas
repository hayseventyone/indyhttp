unit Controller.Request;

interface

uses IdHTTP, IdMultipartFormData, System.Classes, System.SysUtils, IdGlobal ;

function SendPostData(const strUrl, strPatch: String;var respons :TStream): String;

implementation

function SendPostData(const strUrl, strPatch: String ; var respons :TStream ): string;
var
  lHTTP: TIdHTTP;
  sResponse: String;
  Params: TIdMultipartFormDataStream;
begin
  try
    lHTTP := TIdHTTP.Create(nil);
    Params := TIdMultipartFormDataStream.Create;

    Params.AddFile('file', strPatch, 'application/xml');
    lHTTP.Request.AcceptCharSet := 'utf-8';
    try
     lHTTP.Post(strUrl, Params,respons );
     // sResponse :=
      result := (sResponse);
    except
      on E: Exception do
        result := ('Error encountered during POST: ' + E.Message);
    end;
  finally

    Params.Free;
    lHTTP.Free;
  end;
end;

end.
