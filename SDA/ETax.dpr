program ETax;

uses
  Vcl.Forms,
  frmMain in '..\vcl\frmMain.pas' {Form1},
  Vcl.Themes,
  Vcl.Styles,
  Controller.Request in '..\Controller\Controller.Request.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  TStyleManager.TrySetStyle('Carbon');
  Application.CreateForm(TForm1, Form1);
  Application.Run;
end.
